#!/bin/bash
#
# this script scans all lines of the list of sentences file and
# the data dir where files for each DECTalk speakers has been stored and finally
# checks if some number numbers match
#
# Grupo FalaBrasil (2020)
# Universidade Federal do Pará (UFPA)
#
# author: jan 2020
# cassio batista - https://cassota.gitlab.io/

NUM_SPEAKERS=9
SPEAKERS=('paul' 'betty' 'harry' 'frank' 'dennis' 'kit' 'ursula' 'rita' 'wendy')
SOX_PARAMS="-r 11025 -c 1 -b 16 -e signed-integer"

function print_fb_ascii() {
    echo -e "\033[94m  ____                         \033[93m _____     _           \033[0m"
    echo -e "\033[94m / ___| _ __ _   _ _ __   ___  \033[93m|  ___|_ _| | __ _     \033[0m"
    echo -e "\033[94m| |  _ | '__| | | | '_ \ / _ \ \033[93m| |_ / _\` | |/ _\` |  \033[0m"
    echo -e "\033[94m| |_| \| |  | |_| | |_) | (_) |\033[93m|  _| (_| | | (_| |    \033[0m"
    echo -e "\033[94m \____||_|   \__,_| .__/ \___/ \033[93m|_|  \__,_|_|\__,_|    \033[0m"
    echo -e "                  \033[94m|_|      \033[32m ____                _ _\033[0m\033[91m  _   _ _____ ____    _   \033[0m"
    echo -e "                           \033[32m| __ ) _ __ __ _ ___(_) |\033[0m\033[91m| | | |  ___|  _ \  / \          \033[0m"
    echo -e "                           \033[32m|  _ \| '_ / _\` / __| | |\033[0m\033[91m| | | | |_  | |_) |/ ∆ \        \033[0m"
    echo -e "                           \033[32m| |_) | | | (_| \__ \ | |\033[0m\033[91m| |_| |  _| |  __// ___ \        \033[0m"
    echo -e "                           \033[32m|____/|_|  \__,_|___/_|_|\033[0m\033[91m \___/|_|   |_|  /_/   \_\       \033[0m"
    echo -e "                                     https://ufpafalabrasil.gitlab.io/"
}


if test $# -ne 2
then
    print_fb_ascii
    echo "usage: $0 <sent_file> <data_dir>"
    echo "  <sent_file> is the file containing the list sentences, one per line"
    echo "  <data_dir> is the dir passed to util/gen-data.py to store output files"
    exit 1
fi

# check all txt files in data dir to see if each sentence has been
# correctly stored for all 9 speakers
function test_txt() {
    echo -ne "\033[93m"
    echo "[$0] ${FUNCNAME[0]}: check all txt files in data dir to see if"\
         "each sentence has been correctly stored for all"\
         "$NUM_SPEAKERS speakers..."
    echo -ne "\033[0m"
    has_passed=true
    problematic=("") # array of problematic speakers
    for speaker in ${SPEAKERS[@]} ; do
        sent_num=0   # number of sentences in the sentence list file
        sent_count=0 # number of txt files found in the speaker data dir
        # for each line of the file containing the list of sentences
        while read line ; do
            # if a txt file exsits in the user data dir
            # then proceed
            # otherwise add user to problematic list and set test to fail
            filename="${2}/${speaker}/${speaker}_hldata${sent_num}.txt"
            if [[ -f "$filename" ]] ; then
                # if txt content matches the given sentence from the list
                # then increase good txt count
                # otherwise add user to problematic list and set test to fail
                if [[ ! -z "$(grep "$line" $filename)" ]] ; then
                    sent_count=$((sent_count+1)) 
                else
                    if [[ "${problematic[-1]}" != "$speaker" ]] ; then
                        problematic+=("$speaker") # add user to problematic list
                    fi
                    has_passed=false
                fi
            else
                if [[ "${problematic[-1]}" != "$speaker" ]] ; then
                    problematic+=("$speaker")
                fi
                has_passed=false
            fi
            sent_num=$((sent_num+1))
        done < $1
        echo -ne "\r[$0] ${FUNCNAME[0]}: " >&2
        printf "\rspeaker '%-6s' spoke %d of %d sentences" \
               "$speaker" $sent_count $sent_num >&2
    done

    if $has_passed ; then
        echo -ne "\033[32m"
        echo -ne "\r[$0] ${FUNCNAME[0]}: passed! "
        echo -ne "\033[0m"
        echo "all $(wc -l $1 | awk '{print $1}') sentences seem to have been "\
        "spoken by all $NUM_SPEAKERS speakers"
    else
        echo -ne "\033[31m"
        echo -ne "\r[$0] ${FUNCNAME[0]}: failed! "
        echo -ne "\033[0m"
        echo "these speakers are problematic: ${problematic[@]}"
    fi
}

# check if number of files with different extension matches the number of 
# spoken sentences
function test_ext() {
    echo -ne "\033[93m"
    echo "[$0] ${FUNCNAME[0]}: check if number of files with different" \
         "extension matches the number of spoken sentences..."
    echo -ne "\033[0m"
    num_sents=$(wc -l $1 | awk '{print $1}')
    extensions=('hlsyn' 'hlsyn.raw' 'k88' 'k88.mem' 'k88.raw' 'out' 'phon' 'txt')
    has_passed=true
    for speaker in ${SPEAKERS[@]} ; do
        ext_count=()
        for ext in ${extensions[@]} ; do
            count=$(find ${2}/${speaker} -name "${speaker}*.${ext}" | wc -l)
            if [[ $count -ne $num_sents ]] ; then
                has_passed=false
            fi
            ext_count+=("$count")
        done
        printf "\r[%s] %s: speaker '%-6s': " "$0" "${FUNCNAME[0]}" "$speaker" >&2
        echo -ne "\r${ext_count[@]}" >&2
    done
    if $has_passed ; then
        echo -ne "\033[32m"
        echo -ne "\r[$0] ${FUNCNAME[0]}: passed! "
        echo -ne "\033[0m"
        echo "number of files per extension match for all $num_sents sentences"
    else
        echo -ne "\033[31m"
        echo -ne "\r[$0] ${FUNCNAME[0]}: failed! "
        echo -ne "\033[0m"
        echo ""
    fi
}

# check if the number of lines in HLSyn input file matches the number of lines 
# in Klatt input file
function test_frames_hlsyn_klatt() {
    echo -ne "\033[93m"
    echo "[$0] ${FUNCNAME[0]}: check if the number of lines in HLSyn input" \
         "file matches the number of lines in Klatt input file..."
    echo -ne "\033[0m"
    num_sents=$(wc -l $1 | awk '{print $1}')
    has_passed=true
    for i in $(seq 0 $((num_sents-1))) ; do
        for speaker in ${SPEAKERS[@]} ; do
            filebase="${2}/${speaker}/${speaker}_hldata${i}"
            num_hlsyn_lines=$(wc -l "${filebase}.hlsyn" | awk '{print $1}')
            num_klatt_lines=$(wc -l "${filebase}.k88" | awk '{print $1}')
            if [ $num_hlsyn_lines -eq $num_klatt_lines ] ; then
                echo -ne "\rok: ${filebase}.{hlsyn,k88} ($num_hlsyn_lines $num_klatt_lines)" >&2
            else
                printf "\r[%s] %s: '%-6s': " "$0" "${FUNCNAME[0]}" "$speaker"
                echo "num lines in .hlsyn (HLSyn input)"\
                     "and .k88 (Klatt input)"\
                     "differ -> $num_hlsyn_lines $num_klatt_lines ($filebase)"
                has_passed=false
            fi
        done
    done
    if $has_passed ; then
        echo -ne "\033[32m"
        echo -ne "\r[$0] ${FUNCNAME[0]}: passed! "
        echo -ne "\033[0m"
        echo "all .hlsyn and .k88 files from speakers have matching number of frames"
    else
        echo -ne "\033[31m"
        echo -ne "\r[$0] ${FUNCNAME[0]}: failed! "
        echo -ne "\033[0m"
        echo ""
    fi
}

# check if the number of lines in Klatt input file matches the number of audio 
# frames in Klatt output raw file
function test_frames_klatt_raw() {
    echo -ne "\033[93m"
    echo "[$0] ${FUNCNAME[0]}: check if the number of lines in Klatt input" \
         "file matches the number of audio frames in Klatt output raw file..."
    echo -ne "\033[0m"
    if ! type -t sox > /dev/null ; then
        echo "[$0] you cannot run '${FUNCNAME[0]}' without sox installed"
        return
    fi
    num_sents=$(wc -l $1 | awk '{print $1}')
    has_passed=true
    for speaker in ${SPEAKERS[@]} ; do
        for i in $(seq 0 $((num_sents-1))) ; do
            filebase="${2}/${speaker}/${speaker}_hldata${i}"
            num_k88_frames=$(($(wc -l ${filebase}.k88 | awk '{print $1}')-1))
            num_raw_frames=$(($(sox -t raw $SOX_PARAMS ${filebase}.k88.raw -n stat 2>&1 | grep 'Samples read' | awk '{print $3}')/71))
            if [ $num_k88_frames -eq $num_raw_frames ] ; then
                echo -ne "\rok: ${filebase}.{k88,k88.raw} ($num_k88_frames $num_raw_frames)" >&2
            else
                printf "\r[%s] %s: '%-6s': " "$0" "${FUNCNAME[0]}" "$speaker"
                echo "num lines in .k88 (Klatt input) and"\
                     "frames in .k88.raw (Klatt output)"\
                     "differ -> $num_k88_frames $num_raw_frames ($filebase)"
                has_passed=false
            fi
        done
    done
    if $has_passed ; then
        echo -ne "\033[32m"
        echo -ne "\r[$0] ${FUNCNAME[0]}: passed! "
        echo -ne "\033[0m"
        echo "all .k88 and .k88.raw files from all speakers have a" \
             "matching number of frames"
    else
        echo -ne "\033[31m"
        echo -ne "\r[$0] ${FUNCNAME[0]}: failed! "
        echo -ne "\033[0m"
        echo ""
    fi
}

# check if the number of lines in HLSyn input file matches the number of audio
# frames in HLSyn output raw file
function test_frames_hlsyn_raw() {
    echo -ne "\033[93m"
    echo "[$0] ${FUNCNAME[0]}: check if the number of lines in HLSyn input" \
         "file matches the number of audio frames in HLSyn output raw file..."
    echo -ne "\033[0m"
    if ! type -t sox > /dev/null ; then
        echo "[$0] you cannot run '${FUNCNAME[0]}' without sox installed"
        return
    fi
    num_sents=$(wc -l $1 | awk '{print $1}')
    has_passed=true
    for speaker in ${SPEAKERS[@]} ; do
        for i in $(seq 0 $((num_sents-1))) ; do
            filebase="${2}/${speaker}/${speaker}_hldata${i}"
            num_hlsyn_frames=$(($(wc -l ${filebase}.hlsyn | awk '{print $1}')-1))
            num_raw_frames=$(($(sox -t raw $SOX_PARAMS ${filebase}.hlsyn.raw -n stat 2>&1 | grep 'Samples read' | awk '{print $3}')/71))
            if [ $num_hlsyn_frames -eq $num_raw_frames ] ; then
                echo -ne "\rok: ${filebase}.{hlsyn,hlsyn.raw} ($num_hlsyn_frames $num_raw_frames)" >&2
            else
                printf "\r[%s] %s: '%-6s': " "$0" "${FUNCNAME[0]}" "$speaker"
                echo "num lines from .hlsyn (HLSyn input) and" \
                     "audio frames from .hlsyn.raw (HLSyn output)"\
                     "differ -> $num_hlsyn_frames $num_raw_frames ($filebase)"
                has_passed=false
            fi
        done
    done
    if $has_passed ; then
        echo -ne "\033[32m"
        echo -ne "\r[$0] ${FUNCNAME[0]}: passed! "
        echo -ne "\033[0m"
        echo "all .hlsyn and .hlsyn.raw files from all speakers have a" \
             "matching number of frames"
    else
        echo -ne "\033[31m"
        echo -ne "\r[$0] ${FUNCNAME[0]}: failed! "
        echo -ne "\033[0m"
        echo ""
    fi
}

test_txt $1 $2
test_ext $1 $2
test_frames_hlsyn_klatt $1 $2
test_frames_klatt_raw $1 $2
test_frames_hlsyn_raw $1 $2

### EOF ###
