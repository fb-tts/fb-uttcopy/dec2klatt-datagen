#!/bin/bash
#
# this script scans all lines of the list of sentences file and
# checks if the number of lines in the log file correspondent to each sentence
# matches the number of English speakers from DECTalk
#
# Grupo FalaBrasil (2020)
# Universidade Federal do Pará (UFPA)
#
# author: jan 2020
# cassio batista - https://cassota.gitlab.io/

NUM_SPEAKERS=9

if test $# -ne 2
then
    echo "usage: $0 <sent_file> <log_file>"
    echo "  <sent_file> is the file containing the list of sentences, one per line"
    echo "  <log_file> is the log file produced by util/gen-data.py from run.sh"
    exit 1
fi

# test: check if each Harvard sentence has been spoken 9 times
function check_log() {
    echo -ne "\033[93m"
    echo "[$0] ${FUNCNAME[0]}: check log file to see if each sentence"\
         "has been spoken $NUM_SPEAKERS times..."
    echo -ne "\033[0m"
    i=0
    has_passed=true
    while read line ; do
        if [ $(grep "$line" $2 | wc -l) -ne $NUM_SPEAKERS ] ; then
            i=$((i+1))
            has_passed=false
            echo "[$0] ${FUNCNAME[0]} sentence $i might have some problem"
        fi
    done < $1

    if $has_passed ; then
        echo -ne "\033[32m"
        echo -n "[$0] ${FUNCNAME[0]}: passed! "
        echo -ne "\033[0m"
        echo "all $(wc -l $1 | awk '{print $1}') sentences seem to have been"\
             "spoken by all $NUM_SPEAKERS speakers"
    else
        echo -ne "\033[31m"
        echo -n "[$0] ${FUNCNAME[0]}: failed! "
        echo -ne "\033[0m"
        echo "you have $i sentences with problems"
    fi
}

check_log $1 $2
