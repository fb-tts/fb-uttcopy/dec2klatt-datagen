# Pipeline to Generate Synthetic Data in Klatt from DECTalk

Python scripts automatically manipulate mouse and keyboard to manage DECTalk's
GUI, load a text file and proceed through the pipeling until calling Klatt to
generate a raw waveform of synthetic speech given DECTalk's .out parameter file.

## Instructions
Use bash to execute `run.sh`.

It may be wise to separate stdout and stderr, and also write the stdout to a log
file by using bash `tee` command, such as below:

```bash
$ ./run.sh | tee /dev/pts/3 >> log/log2.txt
```

## Description
Script `run.sh` will call the python script `gen-data.py` under the `util/` dir
that will read the list of sentences `.txt` file and create `.out` files via 
DECTalk under the data-dir specified. The python script will then call Dec2HL 
script to convert `.out` files into `.hlsyn` files. HLSyn then converts `.hlsyn` 
into `.k88`, and finally Klatt converts the `.k88` file into a `.raw` audio 
file.

Confusing? See section [Pipeline](#pipeline) below.

## Pipeline
```mermaid
graph LR;
    A(.)             --  text  --> B;
    B[DECTalk]       -- .out   --> C;
    C[Dec2HL]        -- .hlsyn --> D;
    C[Dec2HL]        -. .phon  .-> Z1(.);
    D[HLSyn]         -. .raw   .-> Z2(.);
    D[HLSyn]         -- .k88   --> E;
    E[Klatt]         -- .raw   --> F(.);

    style B fill:#4f4,stroke:#777,stroke-width:4px
    style C fill:#4f4,stroke:#777,stroke-width:4px
    style D fill:#4f4,stroke:#777,stroke-width:4px
    style E fill:#4f4,stroke:#777,stroke-width:4px

    style A  fill:#88f
    style F  fill:#88f
    style Z1 fill:#f44
    style Z2 fill:#f44
```

- DECTalk is a binary file that only works in Windows, but thank god we have Wine
- Dec2HL is a Java script create years ago by former members of FalaBrasil
- HLSyn and Klatt have also been provided to the group when still belonging to LaPS

## Requirements
- Python 3.6+
- Scrot (for pyautogui)
- PyAutoGUI
- PyJNIus (for Dec2HL)
- Wine (for DECTalk)

## FalaBrasil Dependences
Below you can find links where to find all software provided to FalaBrasil in
the past. Beware that they must be cloned into the same folder the python script
to work.

- [DECTalk via speak.exe](https://gitlab.com/fb-tts/fb-uttcopy/dectalk) (follow installation instructions for Wine)
- [Dec2HL](https://gitlab.com/fb-tts/fb-uttcopy/dec2hlsyn) (must be compiled, follow installation instructions)
- [HLSyn and Klatt IO API](https://gitlab.com/fb-tts/fb-uttcopy/hlsyn) (must be compiled, follow installation instructions)


[![FalaBrasil](doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2020)__ - https://ufpafalabrasil.gitlab.io/    
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/    
Cassio Batista - https://cassota.gitlab.io/    
