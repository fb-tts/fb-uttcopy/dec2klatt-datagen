#!/bin/bash
#
# author: jan 2020
# cassio batista - https://cassota.gitlab.io/

LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${HOME}/fb-gitlab/fb-tts/fb-uttcopy/hlsyn/libhlsyn

SPEAKERS=('paul' 'betty' 'harry' 'frank' 'dennis' 'kit' 'ursula' 'rita' 'wendy')
DATA_DIR="data"
SENT_FILE="egs/sent.txt"

rm -rf $DATA_DIR
mkdir -p $DATA_DIR
for spk in ${SPEAKERS[@]}
do
    echo "[$0] generating data for speaker \"$spk\""
    set -x
    python3 util/gen-data.py $spk "$SENT_FILE" "$DATA_DIR"
    set +x
done
