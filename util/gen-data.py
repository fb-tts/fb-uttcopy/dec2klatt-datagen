#!/usr/bin/env python3
#
#             wine                 pyjnius                   C                    C
#         .---------.            .--------.              .-------.            .-------.
# text -> | DECTalk | -> .out -> | Dec2HL | -> .hlsyn -> | HLSyn | -> .k88 -> | Klatt | -> .raw
#         '---------'            '--------'              '-------'            '-------'
#          speak.exe              Dec2HL.py              hlsyn_io.c           klatt_io.c
#
# author: jan 2020
# cassio batista - https://cassota.gitlab.io/

import sys
import os
import subprocess
import pyautogui   # https://github.com/asweigart/pyautogui
import time
import glob
import shutil  # https://stackoverflow.com/questions/11210104/check-if-a-program-exists-from-a-python-script
from termcolor import colored

from paths import *
sys.path.insert(0, DEC2HL_PATH)
from Dec2HL import Dec2HL

AUTO_STOP = True  # if False you have to close DECTalk manually for each speaker
TAG = sys.argv[0]

DECTALK_SPEAKERS = ['Paul', 'Betty', 'Harry', 'Frank', 'Dennis', 'Kit',
                    'Ursula', 'Rita', 'Wendy']


def get_file_len(filename):
    with open(filename, 'r') as f:
        for i, l in enumerate(f):
            pass
    return i + 1


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print_fb_ascii()
        print('usage: %s <speaker> <sent-file> <data-dir>' % TAG)
        print('  <speaker> is one of the 9 DECTalk english speakers')
        print('  <sent-file> is the list of sentences to be loaded into DECTalk')
        print('  <data-dir> directory where speaker dir will be created to save data')
        print()
        print('e.g.: python %s paul sent.txt data/' % TAG)
        print('      out files will be saved under data/paul')
        sys.exit(1)

    if not assert_paths():
        sys.exit(-1)

    # assert args
    speaker = sys.argv[1].lower()
    if speaker.capitalize() not in DECTALK_SPEAKERS:
        print_fb_ascii()
        print('[%s] error: speaker "%s" not available in DECTalk' %
               (TAG, speaker))
        sys.exit(1)

    sent_file = sys.argv[2]
    if not os.path.exists(sent_file):
        print_fb_ascii()
        print('[%s] error: sent file "%s" does not exist' %
               (TAG, sent_file))
        sys.exit(1)
    else:
        src_file = sent_file
        dst_file = os.path.join(ROOT_DIR, os.path.basename(sent_file))
        if os.path.realpath(src_file) != os.path.realpath(dst_file):
            if os.path.exists(dst_file):
                os.remove(dst_file)
            os.symlink(src_file, dst_file)
        sent_file = os.path.basename(dst_file)

    data_dir = os.path.join(sys.argv[3], speaker)
    if os.path.isdir(data_dir):
        print_fb_ascii()
        print('[%s] warning: dir "%s" exists and might be overwritten' %
               (TAG, data_dir))
        time.sleep(1)
    else:
        os.mkdir(data_dir)

    # launch dectalk via wine
    p = subprocess.Popen(DECTALK_CMD)
    time.sleep(5)

    # load sentences file
    pyautogui.hotkey('alt', 'f', interval=0.2)  # lauch file menu
    pyautogui.press('down', interval=0.2)       # open file dialog
    pyautogui.press('enter', interval=0.5)
    pyautogui.write(sent_file, interval=0.1)    # write sentence filename
    pyautogui.press('enter', interval=0.3)

    # switch speaker
    if speaker.capitalize() != DECTALK_SPEAKERS[0]:
        pyautogui.hotkey('alt', 'v', interval=0.2)  # launch voices menu
        for i in range(DECTALK_SPEAKERS.index(speaker.capitalize())):
            pyautogui.press('down', interval=0.1)
        pyautogui.press('enter', interval=0.3)

    # synthesize data by clicking on the speaker button
    x, y = pyautogui.locateCenterOnScreen(os.path.join('res', 
                                                       'speak_button.png'))
    pyautogui.moveTo(x, y)
    time.sleep(0.5)
    pyautogui.click()  # speak!

    print(colored('[%s] info: waiting for DECTalk... ' %
                   TAG, 'yellow', attrs=['bold']), end = ' ')
    sys.stdout.flush()
    num_sents = get_file_len(sent_file)
    if AUTO_STOP:
        num_out_files = len(glob.glob('*.out'))
        while num_out_files < num_sents + 1:
            sys.stderr.write(colored('\r[%s] info: ' % TAG,
                                     'yellow', attrs=['bold']))
            sys.stderr.write(colored('%d / %d .out files created so far...' %
                             (num_out_files, num_sents + 1), 
                             'yellow', attrs=['bold']))
            sys.stderr.flush()
            time.sleep(1)
            num_out_files = len(glob.glob('*.out'))
        sys.stderr.write("\n")
        print(colored(' %d written, done!' % num_out_files, 
                      'yellow', attrs=['bold']))
        sys.stdout.flush()

        # abort synthesis by clicking on the stop button
        print(colored('[%s] info: stoping synthesis' % TAG, 
              'yellow', attrs=['bold']))
        x, y = pyautogui.locateCenterOnScreen(os.path.join('res',
                                                           'stop_button.png'))
        time.sleep(30)
        sys.stdout.flush()
        pyautogui.moveTo(x, y)
        time.sleep(30)
        pyautogui.click()  # stop!

        # close dectalk
        print(colored('[%s] info: closing DECTalk' % TAG,
                      'yellow', attrs=['bold']))
        time.sleep(0.5)
        pyautogui.hotkey('alt', 'f', interval=0.2)  # launch file menu
        pyautogui.press('up', interval=0.2)  # exit
        pyautogui.press('enter', interval=0.5)

    else:
        pyautogui.alert('[%s] info: close DECTalk manually when it stops talking' % TAG)
    p.wait()

    # pre-proc 1: filter .out files and move then to data dir
    print(colored('[%s] info: rearranging dectalk .out files' %
                   TAG, 'yellow', attrs=['bold']))
    sys.stdout.flush()
    num_out_files = len(glob.glob('*.out'))
    if num_out_files < num_sents:
        print(colored('[%s] error: ' % TAG, 'yellow', attrs=['bold']), end=' ')
        print(colored('is not supposed to be lower than sentences',
              'yellow', attrs=['bold']))
        sys.exit(2)
    diff = num_out_files - num_sents
    print('[%s] info: "%d" extra out files: ' % (TAG, diff), end = ' ')
    for i in range(diff):
        print('"hldata%d.out"' % i, end = ' ')
        os.remove('hldata%d.out' % i)
    print(' have been removed')
    with open(sent_file, 'r') as insent:
        for i in range(diff, num_out_files):
            src_file = 'hldata%d.out' % i
            dest_file = os.path.join(data_dir,
                                     '%s_hldata%d' % (speaker, i - diff))
            print('[%s] info: %-16s-> %s:' % (TAG, src_file,
                                              '%s.out' % dest_file), end='\t')
            os.rename(src_file, '%s.out' % dest_file)
            sentence = insent.readline()
            print(sentence.rstrip())
            with open('%s.txt' % dest_file, 'w') as outsent:
                outsent.write(sentence)

    # pre-proc 2: generating .hlsyn files from .out via Dec2HL
    print(colored('[%s] info: Dec2HL:' % TAG, 'yellow', attrs=['bold']), end='')
    print(colored(' gen .hlsyn files from dectalk .out files',
                  'yellow', attrs=['bold']))
    dec2hl = Dec2HL()
    for dectalk_file in glob.glob(os.path.join(data_dir, '*.out')):
        hlsyn_file = dectalk_file.replace('.out', '.hlsyn')
        phone_file = dectalk_file.replace('.out', '.phon')
        spf = str(71)
        arg = '{} {} {} {}'.format(dectalk_file, hlsyn_file, phone_file, spf)
        dec2hl.main(tuple(arg.split()))

    # pre-proc 3: generating .k88 files from .hlsyn via HLSyn
    print(colored('[%s] info: HLSyn:' % TAG, 'yellow', attrs=['bold']), end='')
    print(colored(' gen .k88 files from Dec2HL .hlsyn files', 
                  'yellow', attrs=['bold']))
    for hlsyn_file in glob.glob(os.path.join(data_dir, '*.hlsyn')):
        raw_file = hlsyn_file + '.raw'
        klatt_file = hlsyn_file.replace('.hlsyn', '.k88')
        subprocess.call(HLSYN_CMD.format(HLSYN_CFG_FILE, hlsyn_file, raw_file,
                                         klatt_file).split())

    # pre-proc 4: generating .raw files from .k88 files via Klatt
    print(colored('[%s] info: Klatt:' % TAG, 'yellow', attrs=['bold']), end='')
    print(colored(' gen .raw files from HLSyn .k88 files', 
                  'yellow', attrs=['bold']))
    for klatt_file in glob.glob(os.path.join(data_dir, '*.k88')):
        raw_file = klatt_file + '.raw'
        mem_file = klatt_file + '.mem'
        subprocess.call(KLATT_CMD.format(HLSYN_CFG_FILE, klatt_file, raw_file,
                                         mem_file).split())

    print('[%s] info: finishing up data for speaker "%s"...' % (TAG, speaker))
    time.sleep(1.75)
