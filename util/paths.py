#!/usr/bin/env python3
#
#             wine                 pyjnius                   C                    C
#         .---------.            .--------.              .-------.            .-------.
# text -> | DECTalk | -> .out -> | Dec2HL | -> .hlsyn -> | HLSyn | -> .k88 -> | Klatt | -> .raw
#         '---------'            '--------'              '-------'            '-------'
#          speak.exe              Dec2HL.py              hlsyn_io.c           klatt_io.c
#
# author: jan 2020
# cassio batista - https://cassota.gitlab.io/

import sys
import os
import shutil

def print_fb_ascii():
    print("\033[94m  ____                         \033[93m _____     _           \033[0m")
    print("\033[94m / ___| _ __ _   _ _ __   ___  \033[93m|  ___|_ _| | __ _     \033[0m")
    print("\033[94m| |  _ | '__| | | | '_ \ / _ \ \033[93m| |_ / _` | |/ _` |  \033[0m")
    print("\033[94m| |_| \| |  | |_| | |_) | (_) |\033[93m|  _| (_| | | (_| |    \033[0m")
    print("\033[94m \____||_|   \__,_| .__/ \___/ \033[93m|_|  \__,_|_|\__,_|    \033[0m")
    print("                  \033[94m|_|      \033[32m ____                _ _\033[0m\033[91m  _   _ _____ ____    _ \033[0m")
    print("                           \033[32m| __ ) _ __ __ _ ___(_) |\033[0m\033[91m| | | |  ___|  _ \  / \        \033[0m")
    print("                           \033[32m|  _ \| '_ / _` / __| | |\033[0m\033[91m| | | | |_  | |_) |/ ∆ \       \033[0m")
    print("                           \033[32m| |_) | | | (_| \__ \ | |\033[0m\033[91m| |_| |  _| |  __// ___ \      \033[0m")
    print("                           \033[32m|____/|_|  \__,_|___/_|_|\033[0m\033[91m \___/|_|   |_|  /_/   \_\     \033[0m")
    print("                                     https://ufpafalabrasil.gitlab.io/")


TAG = os.path.basename(sys.argv[0])

SRC_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = os.path.abspath(os.path.dirname(SRC_DIR))

HOME_DIR = os.path.expanduser('~')
FB_GITLAB_PATH = os.path.join(HOME_DIR, 'fb-gitlab', 'fb-tts', 'fb-uttcopy')

DEC2HL_PATH = os.path.join(FB_GITLAB_PATH, 'dec2hlsyn')

WINE_BIN = shutil.which('wine')  # $ which wine
DECTALK_CMD = (WINE_BIN,
               os.path.join(FB_GITLAB_PATH, 'dectalk', 'bin', 'speak.exe'))
HLSYN_CMD = (os.path.join(FB_GITLAB_PATH, 'hlsyn', 'src', 'hlsyn_io') +
             ' -c {} -i {} -o {} -k {}')
KLATT_CMD = (os.path.join(FB_GITLAB_PATH, 'hlsyn', 'src', 'klatt_io') +
             ' -c {} -i {} -o {} -w {}')

HLSYN_CFG_FILE = os.path.join(FB_GITLAB_PATH, 'hlsyn', 'res', 'config.txt')

def assert_paths():
    # assert wine is installed
    if WINE_BIN is None:
        print_fb_ascii()
        print('[%s] error: wine not found.' % TAG)
        print('please install it or fix the path to Wine software')
        return False

    # assert wine has been already configured via 1st run (gecko setup)
    if not os.path.isdir(os.path.join(HOME_DIR, '.wine')):
        print_fb_ascii()
        print('[%s] looks like this is your first wine execution.' % TAG)
        print('please follow the setup procedures then come back here')
        return False

    # assert DECTalk is there
    if not os.path.exists(DECTALK_CMD[1]):
        print_fb_ascii()
        print('[%s] error: file "%s" not found.' % (TAG, DECTALK_CMD[1]))
        print('please fix the path to DECTalk software')
        return False

    # assert libhlsyn is available in ld env var
    lib_found = False
    for path in os.environ['LD_LIBRARY_PATH'].split(':'):
        if 'hlsyn' in path:
            if os.path.exists(os.path.join(path, 'libhlsyn.so')):
                lib_found = True
                break
    if not lib_found:
        print_fb_ascii()
        print('[%s] error: libhlsyn not found in LD_LIBRARY_PATH.' % TAG) 
        print('please include the lib dir in your env var by re-exporting.')
        print(os.environ['LD_LIBRARY_PATH'])
        return False

    # assert hlsyn_io has been compiled
    if not os.path.exists(HLSYN_CMD.split()[0]):
        print_fb_ascii()
        print('[%s] error: file "%s" not found.' % (TAG, HLSYN_CMD.split()[0]))
        print('please compile HLSyn or fix the path to the binary file.')
        return False

    if not os.path.exists(KLATT_CMD.split()[0]):
        print_fb_ascii()
        print('[%s] error: file "%s" not found.' % (TAG, KLATT_CMD.split()[0]))
        print('please compile Klatt or fix the path to the binary file.')
        return False

    return True
